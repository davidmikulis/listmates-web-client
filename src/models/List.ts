export type List = {
  id: number
  name: string
  description: string
  items: ListItem[]
}

export type ListItem = {
  id: number
  name: string
  isComplete: boolean
  dueDate?: Date
}
