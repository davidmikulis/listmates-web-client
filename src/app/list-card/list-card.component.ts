import { Component, Input, OnInit } from '@angular/core'
import { List, ListItem } from 'src/models/List'

@Component({
  selector: 'app-list-card',
  templateUrl: './list-card.component.html',
  styleUrls: ['./list-card.component.scss'],
})
export class ListCardComponent implements OnInit {
  @Input() data!: List

  id: number
  name: string
  description: string
  items: ListItem[]
  constructor() {
    this.id = 0
    this.name = ''
    this.description = ''
    this.items = []
  }

  ngOnInit() {
    this.id = this.data.id
    this.name = this.data.name
    this.description = this.data.description
    this.items = this.data.items
  }

  get listItemsCompleted() {
    return this.data.items.filter((item) => item.isComplete)
  }
}

// TODO: Is there an easier way of passing in a single data object and being able to access
// each key without using the "data." prefix in the template?
// i.e. like passing in parameters in React with the spread operator / destructuring
