import { Component } from '@angular/core'

import { List } from '../models/List'

const sampleData: List[] = [
  {
    id: 1,
    name: 'List One',
    description: 'First List description',
    items: [
      {
        id: 1,
        name: 'List Item One',
        isComplete: true,
      },
      {
        id: 2,
        name: 'List Item Two',
        isComplete: false,
      },
      {
        id: 3,
        name: 'List Item three',
        isComplete: false,
      },
    ],
  },
  {
    id: 2,
    name: 'List Two',
    description: 'Second List description',
    items: [
      {
        id: 4,
        name: 'List Item Four',
        isComplete: true,
      },
      {
        id: 5,
        name: 'List Item Five',
        isComplete: true,
      },
    ],
  },
  {
    id: 3,
    name: 'List Three',
    description: 'Third List description',
    items: [
      {
        id: 6,
        name: 'List Item Six',
        isComplete: false,
      },
      {
        id: 7,
        name: 'List Item Seven',
        isComplete: false,
      },
      {
        id: 8,
        name: 'List Item Eight',
        isComplete: false,
      },
      {
        id: 9,
        name: 'List Item None',
        isComplete: true,
      },
    ],
  },
]

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'Listmates'
  fakeData = sampleData
}
