import { Component, Input, OnInit } from '@angular/core'
import { List } from 'src/models/List'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  @Input() data: List[]

  constructor() {
    this.data = []
  }

  ngOnInit() {}

  executeSelectedChange = (event: any) => {
    console.log(event)
  }
}
